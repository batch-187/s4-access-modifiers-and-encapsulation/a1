<?php require_once './code.php' ?>
<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>S4 - A1</title>
</head>
<body>

   <h1>Building</h1>
   <p><?= $building->getName() ?></p>
   <p><?= $building->getFloors() ?></p>
   <p><?= $building->getAddress() ?></p>
   <p><?= $building->setName("Caswynn Complex") ?></p>
  
   <h1>Condominium</h1>
   <p><?= $condominium->getName() ?></p>
   <p><?= $condominium->getFloors() ?></p>
   <p><?= $condominium->getAddress() ?></p>
   <p><?= $condominium->setName("Enzo Tower") ?></p>

</body>
</html>
